# How to use
It's easy, you just need to have Python 3 installed and use the command line:

`python3 bodyswapRedone.py input_file.txt output_file.txt`

### Input file
The input file must contain at least two names, each one in a different line.

### Output file
The output file will contain a list of random swaps until all combinations are exhausted, and the final result of body(brain).
import random
from itertools import groupby
from math import factorial
import sys

steps = []


class Character:
    """ Class for creating a character """

    def __init__(self, name):
        self.body = name
        self.brain = name

    def swap(self, p2, lock):
        if {self.body, p2.body} in swaps:
            return False
        elif self == p2:
            return False
        elif lock and (self.body == self.brain):
            return False
        else:
            self.brain, p2.brain = p2.brain, self.brain
            swaps.append({self.body, p2.body})

            string = self.brain + " is in " + self.body + "'s body"
            string2 = p2.brain + " is in " + p2.body + "'s body"

            steps.append(string + " and " + string2)
            return True


def print_bodies(characters):
    lista = []
    for char in characters:
        string = char.body + "(" + char.brain + ")"
        lista.append(string)

    steps.append(lista)


def random_swaps(characters, lock=False, show_bodies=False):
    i = 0
    while True:
        result = random.choice(characters).swap(random.choice(characters), lock)
        if show_bodies and result:
            print_bodies(characters)
        i += 1
        if (calculate_combinations(len(characters)) == len(swaps)) or (i > calculate_combinations(len(characters)) * 4):
            break


def create_characters(filename='input.txt'):
    names, extra = read_candidates_from_file(filename)

    candidates = [Character(name) for name in names]
    extra = [Character(e) for e in extra]

    return candidates, extra


def read_candidates_from_file(filename='candidates.txt', separator='#\n', add_extra=False):
    with open(filename) as f:
        content = f.readlines()

        try:
            indx = content.index(separator)
            return [x.strip() for x in content[:(indx - 1)]], [y.strip() for y in content[(indx + 1):]]
        except ValueError:
            return [x.strip() for x in content], []


def calculate_combinations(n, m=2):
    return int(factorial(n) / (factorial(m) * (factorial(n - m))))


def swap_back(character_list, extra, lock=False, show_bodies=False):
    for c in extra:
        character_list.append(c)

    random_swaps(character_list, lock, show_bodies)
    print_bodies(character_list)


def output_to_file(content, filename='output.txt', inline=False):
    with open(filename, 'w') as the_file:
        for item in content:
            if isinstance(item, list):
                if inline:
                    the_file.write(", ".join(item))
                    the_file.write("\n")
                else:
                    the_file.write("\n")
                    the_file.write("\n".join(item))
            else:
                the_file.write("%s\n" % item)


if __name__ == '__main__':
    input_file = sys.argv[1]
    output_file = sys.argv[2]

    swaps = []
    chars, newcomers = create_characters(input_file)
    random_swaps(chars)

    swap_back(chars, newcomers)

    output_to_file(steps, output_file)
